################################################################################
# Package: PyAnalysisUtils
################################################################################

# Declare the package name:
atlas_subdir( PyAnalysisUtils )

# Install files from the package:
atlas_install_python_modules( python/*.py )

